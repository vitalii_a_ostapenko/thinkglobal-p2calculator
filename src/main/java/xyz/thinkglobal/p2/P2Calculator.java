package xyz.thinkglobal.p2;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Interface for implementing counting of p*2 digits numbers with equals p digits at sides
 *
 * @author Vitaliy Ostapenko
 */
public interface P2Calculator<T extends Number> {

    /**
     * Returns count of numbers if Collection contains numbers with p*2 size and p digits at sides are equals
     *
     * @param p       - number of digits to sum at sides
     * @param numbers - collection of numbers to check for p2 numbers
     */
    int count(int p, @NotNull Collection<T> numbers);
}
