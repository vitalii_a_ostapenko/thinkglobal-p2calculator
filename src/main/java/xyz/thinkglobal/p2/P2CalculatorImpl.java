package xyz.thinkglobal.p2;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Implementation for counting of p*2 digits numbers with equals p digits at sides
 *
 * @author Vitaliy Ostapenko
 */
public class P2CalculatorImpl implements P2Calculator<Integer> {

    /**
     * Returns count of numbers if Collection contains numbers with p*2 size and p digits at sides are equals
     *
     * @param p       - number of digits to sum at sides
     * @param numbers - collection of numbers to check for p2 numbers
     * @throws IllegalArgumentException if p <= 0, numbers is null or both
     */
    public int count(int p, @NotNull Collection<Integer> numbers) {
        if (p <= 0) throw new IllegalArgumentException();
        if (numbers.isEmpty()) return 0;
        return (int) numbers.stream()
                .map(Math::abs)
                .map(Number::toString)
                .filter(s -> s.length() == 2 * p)
                .filter(s -> isSumOfPDigitsOnSidesEquals(p, s)).count();
    }

    private boolean isSumOfPDigitsOnSidesEquals(int p, String numberString) {
        int beginSum = calculateNumbersSum(numberString.substring(0, p));
        int endSum = calculateNumbersSum(numberString.substring(p));
        return beginSum == endSum;
    }

    private int calculateNumbersSum(String numberString) {
        return numberString.chars()
                .map(i -> Integer.valueOf(String.valueOf((char) i)))
                .sum();
    }
}
