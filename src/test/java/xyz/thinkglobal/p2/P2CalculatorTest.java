package xyz.thinkglobal.p2;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class P2CalculatorTest {
    private P2Calculator p2Calculator = new P2CalculatorImpl();

    @Test
    void p2NegativeNumbersCount() {
        int p = 2;
        List<Integer> numbers = Arrays.asList(-1212, 1234, 1, 8, -3333, -5124, -1128, -123);
        int expected = 3;
        int actual = p2Calculator.count(p, numbers);
        assertEquals(expected, actual);
    }

    @Test
    void p1Count() {
        int p = 1;
        List<Integer> numbers = Arrays.asList(1, 2, 11, 4, 5, 12);
        int expected = 1;
        int actual = p2Calculator.count(p, numbers);
        assertEquals(expected, actual);
    }

    @Test
    void p2Count() {
        int p = 2;
        List<Integer> numbers = Arrays.asList(1001, 1230, 8034, 12345);
        int expected = 2;
        int actual = p2Calculator.count(p, numbers);
        assertEquals(expected, actual);
    }

    @Test
    void p3Count() {
        int p = 3;
        List<Integer> numbers = Arrays.asList(100100, 123120, 1, 212100, 6);
        int expected = 1;
        int actual = p2Calculator.count(p, numbers);
        assertEquals(expected, actual);
    }

    @Test()
    void p0Count() {
        int p = 0;
        List<Integer> numbers = Arrays.asList(10, 20);
        assertThrows(IllegalArgumentException.class, () -> p2Calculator.count(p, numbers));
    }

    @Test()
    void pEmptyCollectionCount() {
        int p = 2;
        int expected = 0;
        int actual = p2Calculator.count(p, Collections.emptyList());
        assertEquals(expected, actual);
    }

    @SuppressWarnings("ConstantConditions")
    @Test()
    void pNullCollectionCount() {
        int p = 2;
        assertThrows(IllegalArgumentException.class, () -> p2Calculator.count(p, null));
    }
}